module.exports={
  title: "Minds Docs",
  tagline: "The Minds Stack",
  url: "https://developers.minds.com",
  baseUrl: "/",
  organizationName: "Minds",
  projectName: "Minds Docs",
  scripts: [
    "https://buttons.github.io/buttons.js",
    "https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js",
    "/js/code-block-buttons.js"
  ],
  favicon: "img/bulb.png",
  customFields: {
    users: [
      {
        "caption": "Minds.com",
        "image": "/img/bulb.svg",
        "infoLink": "https://www.minds.com",
        "pinned": true
      }
    ]
  },
  onBrokenLinks: "log",
  onBrokenMarkdownLinks: "log",
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        "docs": {
          "routeBasePath": 'docs',
          "path": "docs",
          "showLastUpdateAuthor": true,
          "showLastUpdateTime": true,
          "editUrl": "https://gitlab.com/minds/docs/edit/master/docs/",
          "sidebarPath": require.resolve("./sidebars.js"),
        },
        "blog": {},
        "theme": {
          "customCss": "./src/css/customTheme.css"
        }
      }
    ]
  ],
  plugins: [],
  themeConfig: {
    navbar: {
      "title": "Minds Docs",
      "logo": {
        "src": "img/bulb.svg"
      },
      "items": [
        {
          "href": "https://gitlab.com/minds",
          "label": "Code",
          "position": "left"
        },
        {
          "href": "https://gitcoin.co/minds/",
          "label": "Bounties",
          "position": "left"
        },
        {
          "href": "https://minds.com/",
          "label": "Minds.com",
          "position": "left"
        }
      ],
    },
    colorMode: {
      defaultMode: 'dark',
      disableSwitch: false,
      respectPrefersColorScheme: false,
    },
    image: "img/canyon-hero.jpg",
    footer: {
      links: [],
      copyright: "Copyright © 2023 Minds Inc.",
      logo: {
        src: "img/bulb.svg"
      }
    },
    algolia: {
      appId: "KF5IKKW4RE",
      apiKey: "6abd85a75238d2472246030f433013a8",
      indexName: "developers-minds",
      algoliaOptions: {}
    }
  }
}