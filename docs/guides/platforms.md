---
id: platforms
title: Supported Platforms for Minds
---

Minds supports both responsive web and mobile native application platforms as below.

## Website Platforms

We officially support latest versions of the following browsers on minds.com:

- *Chrome* (https://www.google.com/chrome)  *_Most Stable_
- *Firefox* (https://www.mozilla.org/firefox)
- *Safari* (https://www.apple.com/safari)
- *Edge* (https://microsoft.com/edge)
- *Brave* (https://brave.com)
- Any browser based on Webkit / Chromium engines (Opera, Samsung Internet, UCBrowser, etc.)

The compatible browsers are included with most operating systems as below:

- *Windows - Edge*
- *Android - Chrome*
- *MacOS / iOS - Safari*

*_Please make sure your browser and operating system is up-to-date for the best Minds experience._

## Mobile Application Platforms

We officially support the last 3 operating system versions of Android & iOS on all Android & iOS mobile devices respectively.

*_Please make sure your mobile operating system is up-to-date for the best Minds experience._

_Note: Monetization and NSFW features will be limited when used on mobile devices._
