---
id: tagging
title: Tagging
---

## Tagging

When deploying cloud infrastructure, the following tags should be present on newly created resources:

- `minds-com.module`: the individual component of infrastructure being deployed (eg: `front`, `engine`, `vitess`)
- `minds-com.module-group`: the group of infrastructure comonents this module resides in (eg: `core`, `databases`, `messaging`)
- `minds-com.deployment-method`: deployment method/tooling used to deploy the infrastructure (eg: `terraform`, `manual`, `argocd`)
- `minds-com.environment`: logical environment this module resides in (eg: `sandbox`, `production`)

Not all resources support tags, consult the Terraform resource docs for details regarding your specific resources.

### List of Modules

You can find a list of modules and module groups [here](https://docs.google.com/spreadsheets/d/16saSYUGb85wC4ylLQjJ1G0rXXvxN_TXPAP-_QHW8FX0/edit#gid=397484836).

### Adding New Tags

New tags should generally be added to the `minds-com` tagging keypace within the root compartment. If relevant, add this to the above spreadsheet as well.

## Cost Reporting

### Oracle Cloud

Reports can be created and viewed using the Cost Analysis tool. For more info, see [this doc](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/costanalysisoverview.htm).

### Kubernetes

Cost reporting is currently quite limited within Kubernetes, however the above tags can still be added as [labels](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/) to new resources. When in-cluster reporting is enabled, your resources will then automatically become visible.
