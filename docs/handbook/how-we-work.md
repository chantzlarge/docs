---
id: how-we-work
title: How we work
---

Minds is a distributed team located on both sides of the Atlantic. We communicate via [Minds Chat](https://chat.minds.com) and [Gitlab](https://gitlab.com).

Our workflow aims to provide a continuous and iterative development model that continuously deploys code, drives momentum to reliably ships products, enables developers to be creative and gives product level responsibilities to small teams.

## Issues

### Definition of Ready (DoR)

The **Definition of Ready (DoR)** outlines the requirements that enables an issue to enter the Sprint Backlog. The aim is that everybody understands:

- the goal of the issue
- what needs to be done
- what other issues need to be done before

An issue should meet the following criteria:

- Definition Of Done (DoD): The [DoD](#definition-of-done) should be made clear 
- Acceptance criteria: Everyone should be able to understand what is expected to be delivered 
- Weighted: The development team should provide estimations for the complexity of the issue
- QA: Consideration is given to how the issue will be demoed and tested prior to being deployed
- UX/Design: Sufficient wireframes have been presented and understood by the development team. Full mockups are not required and re-usable components are encouraged.
- Personas: Who does this user impact and why?
- Experiments: Experiments are not required, but it should be made clear if one is expected. If experiments are required, the hypothesis should be outlined. 


### Definition of Done

The **Definition of Done (DoD)** defines when an issue is considered completed and can be closed.

For an issue to be considered Done, it should meet the following criteria:

- The Acceptance Criteria has been met
- Code is tested: Testing includes unit/spec, E2E/automated and manual testing
- Merge requests must clearly outline:
  - What the issue does
  - How the issue can be tested
  - How the issue can be deployed
  - Include a screenshot(s) or a screen recording to demo the issue
- Code has been peer reviewed
- QA has been completed and has been passed
- Code has been moved to the Canary environment

An issue does not need to be:

- Released to end users


## Maintenance

The introduction of new products and features increases our **Tech debt** and requires future capacity to be planned in order to keep products well maintained.

In order to keep on top of the system-wide bug backlog, the list of issues should be treated as a FIFO (First in, first out) queue in order of priority and severity and weights.

- The [Bugs by Severity board](https://gitlab.com/groups/minds/-/boards/1704352?&label_name[]=Type%3A%3ABug) should be used to find issues
- Superhero issues should be resolved immediately
- Urgent bugs should be resolved in no more than 48 hours
- High priority should be first allocated to their respective product labels and validated by their Product Owner
- Take the lowest weighted, oldest created, highest severity bugs first

10-15% of the week should be dedicated to such issues.

---

### Continuous Delivery

#### Checkin to master

All branches should be derived from **master** and should be open for as short a time as possible.
When working on an issue, always try to achieve the **lowest possible scope for regression**. If this isn't possible then try and use a **Feature Flag** to isolate your change.

### Feature flags

**Feature Flags** allow use to check code into production without it being visible to users. We now use Growthbook for our Feature Flags, which can also incorporate Experiments.

### E2E Tests

We use both **Cypress** (web) and **Detox** (mobile) for our E2E tests. We should aim for as full of a coverage as possible. If the tests are too fragile, consider stubbing them.

---

## Communication

### Shape of the week

| Day                                                | Description                                                                                                     | Personnel                               |
| -------------------------------------------------- | --------------------------------------------------------------------------------------------------------------- | --------------------------------------- |
| Daily <br/> _(Tues/Thurs are ASYNC)_ | Scrum calls. <br/> 3 question quick fire. Anything longer should have follow up calls scheduled. <br/>            | Team                                    |
| Weekly                                             | Product check-in. <br/> A short 15-30 minute call to preview work done.                                          | Product owner <br/> Team                 |
| As required                                        | Milestone retrospectives. <br/>Analysis on how the sprint went, viability of the next phases, tech debt analysis | Product owner <br/> Team                 |
| Bi-Weekly                                          | Product planning. <br/> Product / feature proposals <br/> Roadmap planning                                        | Product owners <br/> Product consultants |

### Video Scrums

[Click here](https://www.minds.com/groups/profile/569521254306951168/feed) to join the calls.

Each team has a video call twice per week. The team answers the following questions:

1. What did you yesterday
2. What are you going to do today
3. Are there any issues

### Asynchronous Scrums

On days where the team does not have a video call scheduled, the scrum will be performed offline and still follow the same 3 question patterns as noted above.

### Devshop

Optional daily 30 minute meetings where team members can hop in and out, discuss issues or share knowledge. Monday, Wednesday, Friday 11am EST.

### Todo Feature

- [Gitlab's Todo feature](https://gitlab.com/dashboard/todos) is ideal for keeping in the loop
- Mention a team member in a comment if you need a response
- Try to keep your Todo list as low as possible

---

## Terminology

| Role               | Scope                                                                                                                                                | Personnel                                        |
| ------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------ |
| Product Owner      | Co-ordinates the exploration phase <br/> Regular calls with product teams <br/> Maintains the vision of the product <br/> Controls the maintenance debt | Bill, Mark, Jack                                 |
| Product Consultant | This is a technical role. <br/> Invited by product owners for advice, reviews, estimations, etc                                                       | Mark (tech + code) <br/> Michael (ux&design) <br/> |
| Developer Bench    | Developers are able to be selected by Product Owners to work on their milestones                                                                     | Developers                                       |
| Tech debt          | Bugs / issues from a product                                                                                                                         | N/A                                              |
| Epic               | The product implementation plan, proposal and scope resides                                                                                          | N/A                                              |
| Sprint             | A milestone grouped into the above workflow types. <br/> Exploration, Development, Canary, Release                                                    | N/A                                              |

---

## Weights

Weights should be used as an indication of complexity and not as a metric of time. (See [Gitlabs handbook](https://about.gitlab.com/handbook/engineering/development/dev/create-source-code-be/#capacity-planning) for more information)

| Weight     | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 1: Trivial | The problem is very well understood, no extra investigation is required, the exact solution is already known and just needs to be implemented, no surprises are expected, and no coordination with other teams or people is required.<br/><br/>Examples are documentation updates, simple regressions, and other bugs that have already been investigated and discussed and can be fixed with a few lines of code, or technical debt that we know exactly how to address, but just haven't found time for yet.                                                                                                                                                                                                                   |
| 2: Small   | The problem is well understood and a solution is outlined, but a little bit of extra investigation will probably still be required to realize the solution. Few surprises are expected, if any, and no coordination with other teams or people is required.<br/><br/>Examples are simple features, like a new API endpoint to expose existing data or functionality, or regular bugs or performance issues where some investigation has already taken place.                                                                                                                                                                                                                                                                     |
| 3: Medium  | Features that are well understood and relatively straightforward. A solution will be outlined, and most edge cases will be considered, but some extra investigation will be required to realize the solution. Some surprises are expected, and coordination with other teams or people may be required.<br/><br/>Bugs that are relatively poorly understood and may not yet have a suggested solution. Significant investigation will definitely be required, but the expectation is that once the problem is found, a solution should be relatively straightforward.<br/><br/>Examples are regular features, potentially with a backend and frontend component, or most bugs or performance issues.                               |
| 5: Large   | Features that are well understood, but known to be hard. A solution will be outlined, and major edge cases will be considered, but extra investigation will definitely be required to realize the solution. Many surprises are expected, and coordination with other teams or people is likely required.<br/><br/>Bugs that are very poorly understood, and will not have a suggested solution. Significant investigation will be required, and once the problem is found, a solution may not be straightforward.<br/><br/>Examples are large features with a backend and frontend component, or bugs or performance issues that have seen some initial investigation but have not yet been reproduced or otherwise "figured out". |

---

## Estimates

Gitlab's `/estimate` `/spend` should be used to quantify how long a task will take and has been spent on.

---

## Teamwork

Working as part of a development team does not have to default to traditional backend, frontend and mobile focuses.
A full stack approach brings both knowledge and collaboration to the entire team and also eliminates roadblocks of a backend api not being ready yet for a frontend developer.
Teams should have a shared focus and not shared dependencies.

---

## QA

Please visit the [REAMDE.md](https://gitlab.com/minds/qa) on [https://gitlab.com/minds/qa](https://gitlab.com/minds/qa) for more information about our QA process.

## Issues and Epics

The full project management workflow board can be found [here](https://gitlab.com/groups/minds/-/boards/1264761).

[![Architecture diagram](../assets/work-flow.png "Diagram of Minds Workflow")
Click to enlarge](../assets/work-flow.png)

## Work in progress

This document is a work in progress. Let's discuss how to improve our processes!
