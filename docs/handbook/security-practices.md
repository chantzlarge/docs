---
id: security-practices
title: Security Practices
---

## Password Policy

You should always use a strong password which consists of a minimum length of 12 characters, numbers and special characters.

### Password Managers

Minds uses [Bitwarden](https://bitwarden.com), an open source password manager. You can access Bitwarden by visiting https://vault.bitwarden.com/#/sso and entering 'minds-inc' as the **Organisation identifier**. You are encouraged to use this for your own personal use too.

### Team shared passwords

Teams should only share passwords if absolutely necessary and if the product does not have multi user support. If it is required that teams must share the same login credentials, these must be regularly rotated and **must always** be shared via Bitwarden.

## Two Factor Authentication

You **should** enable two-factor on all applications that support it.

You **must** enable two-factor on your:

- Minds.com account
- Email account
- Minds SSO (Keycloak) account

## Enterprise Single Sign On (Keycloak)

Keycloak is an open source identity and access management solution. It allows for team members to authenticate to applications from a central authority.

### AWS

#### Web console

https://keycloak.minds.com/auth/realms/minds-inc/protocol/saml/clients/amazon-aws

#### CLI

1. You need to install saml2aws by following the instructions [here](https://github.com/Versent/saml2aws#install)
2. Configure by running `saml2aws configure`
   i) Select KeyCloak as the provider
   ii) Type in https://keycloak.minds.com/auth/realms/minds-inc/protocol/saml/clients/amazon-aws as the url
   iii) Enter your username and password
3. Type `saml2aws login` to authenticate
   i) **_NOTE_**: When asked for a role to assume, ensure that you assume the developer role.

### Gitlab

https://gitlab.com/groups/minds/-/saml/sso?token=ZvXxxTY2

### Bitwarden

Visit https://vault.bitwarden.com/#/sso and enter **minds-inc** as the **Organisation identifier**.

### Sentry

https://sentry.io/auth/login/minds-inc/

### GSuite / Google Cloud

Enter your @minds.com email as normal and you will be redirected to Keycloak.

### Keycloak

You can manage your Keycloak account by visting https://keycloak.minds.com/auth/realms/minds-inc/account/#/.

## VPN (Firezone)

You can setup VPN access by visting https://firezone.minds.com/, selecting 'Sign in with Keycloak' and following the steps to setup a new device. 

## Panic email

If a team member lose a device such as a thumb drive, mobile phone, tablet, laptop, etc. that contains their credentials or other sensitive data they should send an email to panic@minds.com right away.
